#include <iostream>
#include <list>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <regex>
#include <climits>

std::unordered_map<unsigned, std::unordered_map<std::string, unsigned>> tramLines; // <numer_kursu, <przystanek, czas>> wczytane kursy
std::map<unsigned, std::pair<unsigned, std::string>> tickets;                      // <czas_ważności, <cena, nazwa_biletu>> bilety
std::unordered_multiset<std::string> unnecessaryTickets;                           // <nazwa biletu> nieopłacalne bilety
unsigned rawNr = 0;                                                                // numer wczytanej linii
unsigned ticketsAmount = 0;                                                        // liczba zaproponowanych biletów

/**
 * Wypisuje błąd.
 * @param line - niepoprawna linia.
 */
void error(const std::string &line) {
    std::cerr << "Error in line " << rawNr << ": " << line << std::endl;
}

/**
 * Gdy, aby zrealizować żądaną trasę trzeba by było czekać.
 * @param stopName - nazwa przystanku, na którym trzeba by było czekać.
 */
void mustWait(const std::string &stopName) {
    std::cout << ":-( " << stopName << std::endl;
}

/**
 * Gdy niemożliwe jest kupno biletów na daną trasę.
 */
void nonviableRequest() {
    std::cout << ":-|" << std::endl;
}

/**
 * Dodanie nowego kursu do zbioru.
 * @param lineNr   - numer kursu;
 * @param tramLineInfo - dane kursu.
 */
void addTramLine(unsigned lineNr, std::unordered_map<std::string, unsigned> &tramLineInfo) {
    tramLines[lineNr] = tramLineInfo;
}

/**
 * Dodaje bilet do zbioru biletów.
 * @param timeLimit - czas ważności;
 * @param price     - cena;
 * @param name      - nazwa biletu.
 */
void addTicket(unsigned timeLimit, unsigned price, const std::string &name, const std::string &line) {

    // Sprawdzenie czy nie ma już biletu o podanej nazwie w unnecessaryTickets.
    if (unnecessaryTickets.find(name) != unnecessaryTickets.end()) {
        error(line);
        return;
    }

    // Sprawdzenie czy nie ma już biletu o podanej nazwie w tickets.
    for (const auto &item : tickets) {
        if (item.second.second == name) {
            error(line);
            return;
        }
    }

    if (tickets.upper_bound(timeLimit) != tickets.end() &&
        tickets.upper_bound(timeLimit)->second.first < price) {

        // Gdy bilet o dłuższej ważności jest tańszy.
        unnecessaryTickets.insert(name);
    }

    std::pair<unsigned, std::string> tmp(price, name);

    auto tmpTicket = tickets.insert({timeLimit, tmp});

    if (!tmpTicket.second) {
        // Gdy bilet o podanym czasie ważności już jest w zbiorze.
        if (tmpTicket.first->second.first < price) {
            // Gdy cena starego biletu jest mniejsza od ceny nowego.
            unnecessaryTickets.insert(name);

            return;
        }
        else {
            // Wstawienie starego biletu do zbioru nieużytecznych.
            unnecessaryTickets.insert(tmpTicket.first->second.second);

            // Wstawienie nowego biletu.
            tickets[timeLimit] = tmp;
        }
    }

    // Gdy został wstawiony nowy bilet usuwamy wszystkie bilety przed nim, które są droższe od niego.
    auto itr = tickets.begin();

    while (itr != tmpTicket.first) {
        if (itr->second.first >= price) {
            unnecessaryTickets.insert(itr->second.second);
            tickets.erase(itr++);
        }
        else ++itr;
    }
}

/**
 * Wyznacza najtańszy zestaw składający się z 2 biletów.
 * @param time - czas podróży.
 * @return koszt i nazwy użytych biletów.
 */
std::pair<unsigned, std::multiset<std::string>> optimizeTwoTickets(unsigned time) {
    // Najlepszy dotychczas wyznaczony zestaw 2 biletów.
    std::pair<unsigned, std::multiset<std::string>> best(UINT_MAX, {""}); // <koszt, zbiór_wykorzystanych_biletów>

    if (2 * tickets.rbegin()->first < time) {
        return best;
    }

    // Ustawienie dwóch iteratorów, jeden na początku, drugi na końcu posortowanego zbioru biletów.
    auto itrLeft = tickets.begin();
    auto itrRight = tickets.rbegin();

    while (itrLeft->first != itrRight->first) {
        // 0 <= itrLeft < itrRight < tickets.size

        if (itrLeft->first + itrRight->first > time) {
            // Aktualna para biletów wystarcza czasowo.

            if (itrLeft->second.first + itrRight->second.first < best.first) {
                // Aktualna para biletów jest najlepsza z dotychczasowych.

                // ustawienie nowego najlepszego kosztu
                best.first = itrLeft->second.first + itrRight->second.first;
                best.second.clear();
                best.second.insert(itrLeft->second.second);
                best.second.insert(itrRight->second.second);
            }
            ++itrRight;
        }
        else {
            ++itrLeft;
        }
    }

    // Sprawdzenie, czy zestaw 2x bilet na miejscu itrLeft, jest lepszy od dotychczas najlepszego zestawu.
    if (2 * itrLeft->first > time && 2 * itrLeft->second.first < best.first) {
        best.first = 2 * itrLeft->second.first;
        best.second.clear();
        best.second.insert(itrLeft->second.second);
        best.second.insert(itrLeft->second.second);
    }

    return best;
}

/**
 * Wyznacza najtańszy zestaw biletów.
 * @param time - czas podróży.
 * @throw length_error, jeśli nie da się kupić biletów na dany czas.
 * @return zbiór biletów.
 */
std::multiset<std::string> optimizeTickets(unsigned time) {
    if (tickets.empty()) {
        throw std::length_error("");
    }
    std::pair<unsigned, std::multiset<std::string>> best; // <koszt, zbiór_biletów> najlepszy dotychczas wyznaczony zestaw

    // Wyznaczenie optymalnego kosztu przy użyciu 1 biletu.
    auto oneTicket = tickets.upper_bound(time);

    if (oneTicket != tickets.end()) {
        best.second.insert(oneTicket->second.second);
        best.first = oneTicket->second.first;
    }
    else {
        best.first = UINT_MAX;
    }

    // Wyznaczenie optymalnego kosztu przy użyciu 2 biletów.
    auto twoTicketsResult = optimizeTwoTickets(time);

    if (twoTicketsResult.first < best.first) {
        // Wyliczony zestaw 2 biletów jest lepszy niż dotychczasowy najlepszy.

        best.second.clear();
        best.first = twoTicketsResult.first;
        best.second.insert(twoTicketsResult.second.begin(), twoTicketsResult.second.end());
    }

    // Wyznaczenie optymalnego kosztu przy użyciu 3 biletów w czasie kwadratowym względem wielkości zbioru tickets.
    for (const auto &item:tickets) {
        // Wyznaczenie optymalnego kosztu dla czasu time - czas aktualnego biletu

        if (time < item.first) {
            // Jeśli czas podróży jest mniejszy niż ważność aktualnego biletu (wszystkie następne
            // będą miały jeszcze dłuższą ważność), to nie ma dalej wyznaczać dla 3 biletów.
            break;
        }
        twoTicketsResult = optimizeTwoTickets(time - item.first);

        if (twoTicketsResult.first != UINT_MAX && item.second.first + twoTicketsResult.first < best.first) {
            // Wyliczony zestaw 3 biletów jest lepszy niż dotychczasowy najlepszy.

            best.second.clear();
            best.first = item.second.first + twoTicketsResult.first;
            best.second.insert(item.second.second);
            best.second.insert(twoTicketsResult.second.begin(), twoTicketsResult.second.end());
        }
    }

    // Nie da się kupić biletów na tę trasę.
    if (best.second.empty()) {
        throw std::length_error("");
    }

    return best.second;
}

/**
 * Sprawdza poprawność trasy i wyzancza czas podróży.
 * @param request - lista z trasą.
 * @throw invalid_argument, jeśli żądana trasa jest niepoprawna.
 * @throw out_of_range, jeśli na jakimś przystanku trzeba czekać.
 * Zakładamy, że trasa "X 1 X" jest niepoprawna.
 * @return czas podróży.
 */
unsigned requestTime(const std::list<std::pair<std::string, unsigned>> &request) {
    auto itr = request.begin();

    unsigned startingTime = 0, lastStopTime = 0;
    std::string stopName = (itr++)->first;

    for (; itr != request.end(); ++itr) {
        auto currentLine = tramLines.find(itr->second);

        if (currentLine == tramLines.end()) {
            // Nie istnieje żądana linia kursu.
            throw std::invalid_argument("");
        }

        auto stop1 = currentLine->second.find(stopName);
        auto stop2 = currentLine->second.find(itr->first);

        if (stop1 == currentLine->second.end() || stop2 == currentLine->second.end() || stop1 == stop2) {
            // Nie ma któregoś z przystanków na tej linii lub dwa kolejne wczytane przystanki to ten sam przystanek.
            throw std::invalid_argument("");
        }

        if (startingTime == 0) {
            // Pierwszy obrót pętli.
            startingTime = stop1->second;
            lastStopTime = stop1->second;
        }

        if (stop1->second < lastStopTime || stop1->second > stop2->second) {
            // Tramwaj odjeżdża za wcześnie lub linia jest skierowana w przeciwnym kierunku.
            throw std::invalid_argument("");
        }

        if (stop1->second > lastStopTime) {
            // Trzeba czekać.
            throw std::out_of_range(stop1->first);
        }

        lastStopTime = stop2->second;
        stopName = stop2->first;
    }

    return lastStopTime - startingTime;
}

/**
 * Wyszukuje optymalny zestaw biletów.
 * @param request - lista z trasą
 * @param line    - wczytana linia
 * pair.first     - nazwa przystanku
 * pair.second    - numer linii
 */
void ticketsQuery(const std::list<std::pair<std::string, unsigned>> &request, const std::string &line) {
    try {
        auto result = optimizeTickets(requestTime(request));

        if (!result.empty()) {
            ticketsAmount += result.size();

            auto itr = result.begin();

            std::cout << "! " << *itr++;

            for (; itr != result.end(); ++itr) {
                std::cout << "; " << *itr;
            }

            std::cout << std::endl;
        }
    }
    catch (std::out_of_range &out_of_range) {
        // Trzeba czekać.
        mustWait(out_of_range.what());
    }
    catch (std::length_error &length_error) {
        // Nie da się kupić biletów na tę trasę.
        nonviableRequest();
    }
    catch (std::invalid_argument &invalid_argument) {
        // Niopoprawna trasa.
        error(line);
        return;
    }
}

/**
 * Funkcja przekształca stringa zawierającego same cyfry na unsigned int.
 * @param strNumber - liczba zapisana w stringu
 * @throw invalid_argument, jeśli liczba w stringu nie mieści się w zakresie unsigned int.
 * @return liczbę przekształconą do unsigned int.
 */
unsigned stringToU(const std::string &strNumber) {
    unsigned long long number = 0;

    for (auto &character : strNumber) {
        number = number * 10 + (character - '0');
        if (number > UINT_MAX)
            throw std::invalid_argument(strNumber);
    }

    return (unsigned) number;
}

/**
 * Funkcja przekształca czas zawarty w stringu do czasu w minutach zapisanego jako unsigned int.
 * @param strTime - czas zapisany w stringu
 * @throw invalid_argument, jeśli czas nie mieści się w zakresie od 5:55 do 21:21.
 * @return czas w minutach zapisany jako unsigned int.
 */
unsigned timeToU(const std::string &strTime) {
    unsigned long long time = 0;
    int i;

    // Przekształcenie do unsigned godzin.
    for (i = 0; strTime.at(i) != ':'; ++i) {
        time = time * 10 + (strTime.at(i) - '0');

        if (time > UINT_MAX)
            throw std::invalid_argument(strTime);
    }

    // Przekształcenie do unsigned minut.
    time = time * 60 + (strTime.at(i + 1) - '0') * 10 + (strTime.at(i + 2) - '0');

    if (time > 21 * 60 + 21 || time < 5 * 60 + 55)
        throw std::invalid_argument(strTime);

    return (unsigned) time;
}

/**
 * Funkcja przekształca cenę zawartą w stringu do ceny w groszach zapisanej jako unsigned int.
 * @param strPrice - cena zapisana w stringu
 * @throw invalid_argument, jeśli cena nie mieści się w zakresie unsigned int lub wynosi 0.
 * @return cena w groszach zapisana jako unsigned int.
 */
unsigned priceToU(const std::string &strPrice) {
    unsigned long long price = 0;
    int i;

    // Przeksztalcenie do unsigned złotówek.
    for (i = 0; strPrice.at(i) != '.'; ++i) {
        price = price * 10 + (strPrice.at(i) - '0');
        if (price > UINT_MAX)
            throw std::invalid_argument(strPrice);
    }

    // Przekształcenie do unsigned groszy.
    for (int j = 1; j <= 2; j++) {
        price = price * 10 + (strPrice.at(j + i) - '0');
        if (price > UINT_MAX)
            throw std::invalid_argument(strPrice);
    }

    if (price == 0)
        throw std::invalid_argument(strPrice);

    return (unsigned) price;
}

/**
 * Funkcja dzieli stringa według separatora, którym jest spacja.
 * @param str  - string, który ma być podzielony
 * @param cont - vector, do którego zapisujemy podziały stringa
 */
void split(const std::string &str, std::vector<std::string> &vector) {
    const std::string splitPattern = " ";
    std::size_t current = str.find_first_of(splitPattern);
    std::size_t previous = 0;

    while (current != std::string::npos) {
        vector.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find_first_of(splitPattern, previous);
    }
    vector.push_back(str.substr(previous, current - previous));
}

/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę dodania nowej linii i jeśli tak, to ją wykonuje.
 * @param line        - wczytana linia
 * @param newTramLine - wzorzec na komendę dodania nowej linii
 * @return true, jeśli jest to poprawna komenda dodania nowej linii i false wpp.
 */
bool tryAddTramLine(std::string &line) {
    const std::regex newTramLine(
            R"(^(\d+) ((?:(?:[1-9]\d|\d):[0-5]\d [a-zA-Z_^]+)(?: (?:[1-9]\d|\d):[0-5]\d [a-zA-Z_^]+)*)$)");
    unsigned lineNr;
    unsigned time;
    unsigned previousTime = 0;
    std::string name;
    std::smatch result;
    std::vector<std::string> timesAndNames;    // czasy odjazdow z przystanków i nazwy przystanków
    std::unordered_map<std::string, unsigned> lineInfo;

    if (std::regex_search(line, result, newTramLine)) {
        try {
            lineNr = stringToU(result[1]);

            // Sprawdzenie czy istnieje już linia o wczytanym numerze.
            if (tramLines.find(lineNr) != tramLines.end()) {
                return false;
            }
            split(result[2], timesAndNames);

            // Linia musi zawierać co najmniej jeden przystanek.
            if (timesAndNames.empty())
                return false;

            // Dodawanie do lineInfo pary czas odjazdu z przystanku i nazwa przystanku, jeżeli czas odjazdu z przystanku
            // nie mieści sie w unsigned to funkcja timeToU wyrzuca wyjątek invalid_argument.
            for (unsigned long i = 0; i < timesAndNames.size(); i += 2) {
                time = timeToU(timesAndNames[i]);
                name = timesAndNames[i + 1];

                // Sprawdzenie, czy czasy są rosnące i czy nazwy się nie powtarzają.
                if (time <= previousTime || lineInfo.find(name) != lineInfo.end())
                    return false;

                lineInfo.insert(std::pair<std::string, unsigned>(name, time));
                previousTime = time;
            }

            addTramLine(lineNr, lineInfo);
            return true;
        }
        catch (...) {
            return false;
        }
    }

    return false;
}

/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę dodania nowego biletu i jeśli tak, to ją wykonuje.
 * @param line      - wczytana linia
 * @param newTicket - wzorzec na komendę dodania nowego biletu
 * @return true, jeśli jest to poprawna komenda dodania nowego biletu i false wpp.
 */
bool tryAddTicket(std::string &line) {
    const std::regex newTicket(R"(^([a-zA-Z ]+) ((?:[1-9]\d*|\d)\.\d{2}) ([1-9]\d*)$)");
    unsigned long price;
    unsigned long time;
    std::string ticketName;
    std::smatch result;

    if (std::regex_search(line, result, newTicket)) {
        try {
            ticketName = result[1];
            price = priceToU(result[2]);
            time = stringToU(result[3]);

            addTicket(time, price, ticketName, line);
            return true;
        }
        catch (...) {
            return false;
        }
    }

    return false;
}

/**
 * Funkcja sprawdza, czy podana linia zawiera poprawną komendę pytania o bilety i jeśli tak, to ją wykonuje.
 * @param line            - wczytana linia
 * @param newTicketsQuery - wzorzec na komendę pytania o bilety
 * @return true, jeśli jest to poprawna komenda pytania o bilety i false wpp.
 */
bool tryTicketsQuery(std::string &line) {
    const std::regex newTicketsQuery(R"(^\? ([a-zA-Z_^]+) ((?:\d+ [a-zA-Z_^]+)(?: \d+ [a-zA-Z_^]+)*)$)");
    unsigned long lineNr;
    std::string firstStop;
    std::string stop;
    std::smatch result;
    std::vector<std::string> numbersAndStops;              // numery kursów i nazwy przystanków bez pierwszego przystanku
    std::list<std::pair<std::string, unsigned>> request;

    if (std::regex_search(line, result, newTicketsQuery)) {
        try {

            firstStop = result[1];
            split(result[2], numbersAndStops);

            // Pytanie o kurs musi zawierać co najmniej dwa przystanki, z czego nazwa pierwszego jest zapisywana
            // do zmiennej firstStop.
            if (numbersAndStops.empty())
                return false;

            request.emplace_back(firstStop, 0);

            // Dodawanie do request pary numer kursu i nazwa przystanku, jeżeli numer przystanku nie mieści się
            // w unsigned to funkcja stringToU wyrzuca wyjątek invalid_argument.
            for (unsigned long i = 0; i < numbersAndStops.size(); i += 2) {
                lineNr = stringToU(numbersAndStops[i]);
                stop = numbersAndStops[i + 1];

                request.emplace_back(stop, lineNr);
            }

            ticketsQuery(request, line);
            return true;
        }
        catch (...) {
            return false;
        }
    }

    return false;
}

int main() {
    std::string line;

    while (getline(std::cin, line)) {
        ++rawNr;
        // Jeżeli linia nie jest pusta, a polecenie nie jest poprawne, to wypisywany jest wiersz z informacją o błędzie.
        if (!line.empty() && !tryAddTicket(line) && !tryAddTramLine(line)
            && !tryTicketsQuery(line))
            error(line);
    }

    std::cout << ticketsAmount << std::endl;
    return 0;
}