#ifndef MD406127_MS406322_FIBO_H
#define MD406127_MS406322_FIBO_H

#include <vector>
#include <string>
#include <iostream>
#include <cassert>

class Fibo {
private:
    std::vector<bool> normalized_form;

    static bool correctString(const char *);

    void normalize();

public:
    Fibo(); // Tworzy zero.

    explicit Fibo(const char *); // Tworzy obiekt z napisu.
    explicit Fibo(std::string s): Fibo(&s[0]) {};

    // Zakładamy, że maxint to największa wartość, którą przyjmuje
    // konstruktor z liczby całkowitej.
    Fibo(int n);

    Fibo(unsigned int) = delete;

    Fibo(long) = delete;

    Fibo(long long) = delete;

    Fibo(unsigned long) = delete;

    Fibo(unsigned long long) = delete;

    Fibo(float) = delete;

    Fibo(double) = delete;

    Fibo(char) = delete;

    Fibo(bool) = delete;


    Fibo(const Fibo &);

    Fibo(Fibo &&) noexcept;

    size_t length() const;

    friend bool operator<(const Fibo &lhs, const Fibo &rhs);

    friend bool operator==(const Fibo &lhs, const Fibo &rhs);

    friend std::ostream &operator<<(std::ostream &out, const Fibo &fibo);

    friend Fibo operator<<(Fibo lhs, int n);

    Fibo &operator+=(const Fibo &rhs);

    Fibo &operator&=(const Fibo &rhs);

    Fibo &operator|=(const Fibo &rhs);

    Fibo &operator^=(const Fibo &rhs);

    Fibo &operator<<=(int n);

    Fibo &operator=(const Fibo &rhs);

    Fibo &operator=(Fibo &&rhs) noexcept;
};
const Fibo &Zero();

const Fibo &One();

bool operator>(const Fibo &lhs, const Fibo &rhs);

bool operator<=(const Fibo &lhs, const Fibo &rhs);

bool operator>=(const Fibo &lhs, const Fibo &rhs);

bool operator!=(const Fibo &lhs, const Fibo &rhs);

std::ostream &operator<<(std::ostream &out, const Fibo &fibo);

Fibo operator+(Fibo lhs, const Fibo &rhs);

Fibo operator&(Fibo lhs, const Fibo &rhs);

Fibo operator|(Fibo lhs, const Fibo &rhs);

Fibo operator^(Fibo lhs, const Fibo &rhs);

Fibo operator<<(Fibo lhs, int n);

#endif //MD406127_MS406322_FIBO_H
