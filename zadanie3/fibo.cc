#include <iostream>
#include <algorithm>

#include "fibo.h"

Fibo::Fibo() : normalized_form(1, false) {}

bool Fibo::correctString(const char *str) {
    if (!str)                           // NULL
        return false;

    if (str[0] == '0' && str[1] == '\0') // Napis "0"
        return true;

    if (str[0] == '0')                  // Napis nie może zaczynać się zerem.
        return false;

    int i = 1;
    while (str[i] != '\0') {            // Napis zawiera tylko 0 i 1.
        if (str[i] != '0' && str[i] != '1') {
            return false;
        }
        i++;
    }
    return true;
}

// Z postaci niekoniecznie unormowanej liczby robi postać unormowaną.
void Fibo::normalize() {
    // Odwracam w celu szybszego pop'owania zer wiodących.
    std::reverse(normalized_form.begin(), normalized_form.end());
    normalized_form.push_back(false); // Wrzucam "strażnika" dla ułatwienia.
    for (size_t i = normalized_form.size() - 1; i > 0; i--) {
        if (normalized_form[i - 1]) {
            break;
        } else {
            normalized_form.pop_back(); // Usuwamy wiodące zera.
        }
    }
    if (normalized_form.size() >= 2) {
        for (size_t i = normalized_form.size() - 2; i > 0; i--) {
            for (size_t j = i; normalized_form[j] && normalized_form[j - 1]
                    && j < normalized_form.size(); j += 2) {
                normalized_form[j] = false;
                normalized_form[j - 1] = false;
                normalized_form[j + 1] = true;
            }
        }
    }
    if (!normalized_form[normalized_form.size() - 1] && normalized_form.size() != 1) {
        normalized_form.pop_back();
    }
    std::reverse(normalized_form.begin(), normalized_form.end());
}

Fibo::Fibo(int n) {
    assert(n >= 0);
    static std::vector<long> already_calculated = {1, 2};
    // Ewentualnie doliczamy potrzebne liczby.
    while (n > already_calculated[already_calculated.size() - 1]) {
        already_calculated.push_back(already_calculated[already_calculated.size() - 1]
        + already_calculated[already_calculated.size() - 2]);
    }
    auto it = std::upper_bound(already_calculated.begin(), already_calculated.end(), n);
    --it;
    // To staje się indeksem pierwszej mniejszej/rownej liczby fibo w tablicy.
    int index = it - already_calculated.begin();
    if (index == -1) {
        normalized_form.push_back(false);
    } else {
        normalized_form.resize(index + 1, false);
        //normalized_form = std::vector<bool>(index + 1, false);
        normalized_form[index] = true;
        n -= already_calculated[index];
        while (n > 0) {
            it = std::upper_bound(already_calculated.begin(), already_calculated.end(), n);
            --it;
            index = (it - already_calculated.begin());
            normalized_form[index] = true;
            n -= already_calculated[index];
        }
        std::reverse(normalized_form.begin(), normalized_form.end());
    }
}

Fibo::Fibo(const char *str) {
    assert(correctString(str));
    std::string s = str;
    normalized_form.resize(s.size());
    int j = 0;
    for (size_t i = 0; i < s.size(); i++) {
        if (s[i] == '0') {
            normalized_form[j] = false;
        } else {
            normalized_form[j] = true;
        }
        j++;
    }
    this->normalize();
}

Fibo::Fibo(const Fibo &rhs) {
    this->normalized_form = rhs.normalized_form;
}

Fibo::Fibo(Fibo &&rhs) noexcept {
    this->normalized_form = std::move(rhs.normalized_form);
}

size_t Fibo::length() const {
    return normalized_form.size();
}

const Fibo &Zero() {
    static const Fibo zero = Fibo(0);
    return zero;
}

const Fibo &One() {
    static const Fibo one = Fibo(1);
    return one;
}

bool operator<(const Fibo &lhs, const Fibo &rhs) {
    if (lhs.length() < rhs.length()) {
        return true;
    } else if (lhs.length() > rhs.length()) {
        return false;
    } else {
        return lhs.normalized_form < rhs.normalized_form;
    }
}

bool operator>(const Fibo &lhs, const Fibo &rhs) {
    return rhs < lhs;
}

bool operator<=(const Fibo &lhs, const Fibo &rhs) {
    return !(lhs > rhs);
}

bool operator>=(const Fibo &lhs, const Fibo &rhs) {
    return !(lhs < rhs);
}

bool operator==(const Fibo &lhs, const Fibo &rhs) {
    return lhs.normalized_form == rhs.normalized_form;
}

bool operator!=(const Fibo &lhs, const Fibo &rhs) {
    return !(lhs == rhs);
}

Fibo &Fibo::operator=(const Fibo &rhs) {
    this->normalized_form = rhs.normalized_form;

    return *this;
}

Fibo &Fibo::operator=(Fibo &&rhs) noexcept {
    this->normalized_form = std::move(rhs.normalized_form);

    return *this;
}

std::ostream &operator<<(std::ostream &out, const Fibo &fibo) {
    for (auto const &value : fibo.normalized_form) {
        out << value;
    }

    return out;
}

Fibo &Fibo::operator+=(const Fibo &rhs) {
    if (this->length() <= rhs.length()) {
        size_t difference = rhs.length() - this->length();
        this->normalized_form.resize(rhs.length() + 1);
        std::rotate(this->normalized_form.begin(),
                this->normalized_form.end() - difference - 1,
                this->normalized_form.end());
    }

    size_t difference = this->length() - rhs.length();
    bool buffer[] = {false, false};
    bool next = true;

    for (size_t i = 0; i < rhs.normalized_form.size(); ++i) {
        if (!next) {
            buffer[0] = buffer[1];
            buffer[1] = false;
            next = true;
        } else if (buffer[0] && rhs.normalized_form[i] &&
                   this->normalized_form[i + difference]) {
            this->normalized_form[i + difference - 1] = true;

            if (i < rhs.length() - 2) {
                buffer[0] = buffer[1];
                buffer[1] = true;
            } else if (i == rhs.length() - 2) {
                buffer[0] = true;
                buffer[1] = false;
            }
        } else if (rhs.normalized_form[i] && this->normalized_form[i + difference]) {
            this->normalized_form[i + difference - 1] = true;
            this->normalized_form[i + difference] = false;

            if (i < rhs.length() - 2) {
                buffer[0] = buffer[1];
                buffer[1] = true;
            } else if (i == rhs.length() - 2) {
                buffer[0] = true;
                buffer[1] = false;
            }
        } else if (buffer[0] &&
                   (rhs.normalized_form[i] || this->normalized_form[i + difference])) {
            if (i == rhs.length() - 1) {
                this->normalized_form[i + difference - 1] = true;
                this->normalized_form[i + difference] = false;
            } else if (this->normalized_form[i + difference + 1] || rhs.normalized_form[i + 1]) {
                this->normalized_form[i + difference - 1] = true;
                this->normalized_form[i + difference] = false;
                buffer[0] = buffer[1];
                buffer[1] = false;
                next = false;
            } else {
                this->normalized_form[i + difference - 1] = true;
                this->normalized_form[i + difference] = false;

                if (i < rhs.length() - 2) {
                    buffer[0] = buffer[1];
                    buffer[1] = true;
                } else {
                    buffer[0] = true;
                    buffer[1] = false;
                }
            }
        } else {
            if (buffer[0] || rhs.normalized_form[i]) {
                this->normalized_form[i + difference] = true;
            }
            buffer[0] = buffer[1];
            buffer[1] = false;
        }
    }
    this->normalize();
    return *this;
}

Fibo &Fibo::operator&=(const Fibo &rhs) {
    if (this->length() > rhs.length()) {
        size_t difference = this->length() - rhs.length();

        std::rotate(this->normalized_form.begin(),
                this->normalized_form.begin() + difference,
                this->normalized_form.end());
        this->normalized_form.resize(rhs.length());
    }

    size_t difference = rhs.length() - this->length();

    for (size_t i = 0; i < this->normalized_form.size(); ++i) {
        if (!rhs.normalized_form[i + difference] || !this->normalized_form[i])
            this->normalized_form[i] = false;
    }
    this->normalize();
    return *this;
}

Fibo &Fibo::operator|=(const Fibo &rhs) {
    if (this->length() < rhs.length()) {
        size_t difference = rhs.length() - this->length();

        this->normalized_form.resize(rhs.length());
        std::rotate(this->normalized_form.begin(),
                this->normalized_form.end() - difference,
                this->normalized_form.end());
    }

    size_t difference = this->length() - rhs.length();

    for (size_t i = 0; i < rhs.normalized_form.size(); ++i) {
        if (rhs.normalized_form[i])
            this->normalized_form[i + difference] = true;
    }
    this->normalize();
    return *this;
}

Fibo &Fibo::operator^=(const Fibo &rhs) {
    if (this->length() < rhs.length()) {
        size_t difference = rhs.length() - this->length();

        this->normalized_form.resize(rhs.length());
        std::rotate(this->normalized_form.begin(),
                this->normalized_form.end() - difference,
                this->normalized_form.end());
    }

    size_t difference = this->length() - rhs.length();

    for (size_t i = 0; i < rhs.normalized_form.size(); ++i) {
        this->normalized_form[i + difference] =
                (rhs.normalized_form[i] && !this->normalized_form[i + difference]) ||
                (!rhs.normalized_form[i] && this->normalized_form[i + difference]);
    }
    this->normalize();
    return *this;
}

Fibo &Fibo::operator<<=(int n) {
    this->normalized_form.resize(length() + n);

    return *this;
}

Fibo operator+(Fibo lhs, const Fibo &rhs) {
    return lhs += rhs;
}

Fibo operator&(Fibo lhs, const Fibo &rhs) {
    return lhs &= rhs;
}

Fibo operator|(Fibo lhs, const Fibo &rhs) {
    return lhs |= rhs;
}

Fibo operator^(Fibo lhs, const Fibo &rhs) {
    return lhs ^= rhs;
}

Fibo operator<<(Fibo lhs, int n) {
    return lhs <<= n;
}

