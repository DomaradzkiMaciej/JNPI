/*
 * fibin.h
 */

#ifndef FIBIN_H_
#define FIBIN_H_

#include <iostream>
#include <type_traits>

/*
 * bases.h
 */

template<typename ValueType>
struct Expression {};

struct Condition {};

struct Function {};

/* end of bases.h */

/*
 * lit.h
 */

template<std::size_t n>
struct Fib
{
    template<typename ValueType>
    struct InFib
    {
        static constexpr ValueType value =
            Fib<n - 1>::template InFib<ValueType>::value
                + Fib<n - 2>::template InFib<ValueType>::value;
    };
};

template<>
struct Fib<1>
{
    template<typename ValueType>
    struct InFib
    {
        static constexpr ValueType value = 1;
    };
};

template<>
struct Fib<0>
{
    template<typename ValueType>
    struct InFib
    {
        static constexpr ValueType value = 0;
    };
};

template<typename>
struct is_Fib : std::false_type {};

template<std::size_t n>
struct is_Fib<Fib<n>> : std::true_type {};

template<typename F>
struct Lit
{
    static_assert(is_Fib<F>::value);

    template<typename ValueType, typename List>
    struct Expr : Expression<ValueType>
    {
        typedef Expr<ValueType, List> TrueType;

        static constexpr ValueType value()
        {
            return F::template InFib<ValueType>::value;
        }
    };
};

struct True {};
struct False {};

template<>
struct Lit<True>
{
    template<typename ValueType, typename List>
    struct Expr : Condition
    {
        typedef Expr<ValueType, List> TrueType;

        static constexpr bool value()
        {
            return true;
        }
    };
};

template<>
struct Lit<False>
{
    template<typename ValueType, typename List>
    struct Expr : Condition
    {
        typedef Expr<ValueType, List> TrueType;

        static constexpr bool value()
        {
            return false;
        }
    };
};

/* end of lit.h */

/*
 * sum.h
 */

template<typename ...Exprs>
struct Sum
{
    template<typename ValueType, typename List>
    struct Expr : Expression<ValueType>
    {
        typedef Expr<ValueType, List> TrueType;

        static constexpr ValueType value()
        {
            static_assert(sizeof...(Exprs) >= 2);
            static_assert((std::is_base_of<Expression<ValueType>,
                           typename Exprs::template Expr<ValueType, List>::TrueType>::value && ...));

            return (Exprs::template Expr<ValueType, List>::TrueType::value() + ...);
        }
    };
};

template<typename Exp>
struct Inc1
{
    template<typename ValueType, typename List>
    struct Expr : Expression<ValueType>
    {
        typedef Expr<ValueType, List> TrueType;

        static constexpr ValueType value()
        {
            static_assert(std::is_base_of<Expression<ValueType>,
                          typename Exp::template Expr<ValueType, List>::TrueType>::value);

            return Sum<Exp, Lit<Fib<1>>>::template Expr<ValueType, List>::TrueType::value();
        }
    };
};

template<typename Exp>
struct Inc10
{
    template<typename ValueType, typename List>
    struct Expr : Expression<ValueType>
    {
        typedef Expr<ValueType, List> TrueType;

        static constexpr ValueType value()
        {
            static_assert(std::is_base_of<Expression<ValueType>,
                          typename Exp::template Expr<ValueType, List>::TrueType>::value);

            return Sum<Exp, Lit<Fib<10>>>::template Expr<ValueType, List>::TrueType::value();
        }
    };
};

/* end of sum.h */

/*
 * if.h
 */

template<typename Cond, typename Then, typename Else>
struct If
{
    template<typename ValueType, typename List>
    struct Expr
    {
        typedef typename Cond::template Expr<ValueType, List>::TrueType C;
        typedef typename Then::template Expr<ValueType, List>::TrueType T;
        typedef typename Else::template Expr<ValueType, List>::TrueType E;

        static_assert(std::is_base_of<Condition, C>::value);

        typedef typename std::conditional<C::value(), T, E>::type TrueType;
    };
};

/* end of if.h */

/*
 * eq.h
 */

template <typename Left, typename Right>
struct Eq
{
    template<typename ValueType, typename List>
    struct Expr : Condition
    {
        typedef Expr<ValueType, List> TrueType;

        static constexpr bool value()
        {
            typedef typename Left::template Expr<ValueType, List>::TrueType L;
            typedef typename Right::template Expr<ValueType, List>::TrueType R;

            static_assert((std::is_base_of<Expression<ValueType>, L>::value
                           && std::is_base_of<Expression<ValueType>, R>::value)
                             || (std::is_base_of<Condition, L>::value
                                 && std::is_base_of<Condition, R>::value));

            return (L::value() == R::value());
        }
    };
};

/* end of eq.h */

/*
 * var.h
 */

typedef unsigned int hash_t;

constexpr int length(const char *c)
{
    for (int i = 0; i <= 6; ++i)
    {
        if (c[i] == '\0')
            return i;
    }
    return 7;
}

constexpr bool validCstring(const char *c) {
    for (int i = 0; c[i] != '\0'; ++i) {
        if (!('0' <= c[i] && c[i] <= '9')
            && !('a' <= c[i] && c[i] <= 'z')
            && !('A' <= c[i] && c[i] <= 'Z'))
            return false;
    }

    return true;
}

constexpr hash_t charHash(const char c)
{
    if (c >= '0' && c <= '9')
        return c - '0' + 1; // Zero is for not existing char.
    else if (c >= 'a' && c <= 'z')
        return c - 'a' + 11; // The first ten ints are for '0'-'9'.
    else// (c >= 'A' && c <= 'Z')
        return c - 'A' + 11; // The first ten ints are for '0'-'9'.
}

constexpr hash_t hash(const char *c, const unsigned short i)
{
    if (c[i] == '\0')
        return 0;
    else
        return charHash(c[i]) + 37 * hash(c, i + 1);
}

constexpr hash_t Var(const char *c)
{
    if(length(c) > 6 || !validCstring(c))
        return 0;

    return hash(c, 0);
}

/* end of var.h */

/*
 * list.h
 */

struct EmptyNode {};

template<hash_t h, typename Value, typename Tail>
struct Node
{
    static constexpr hash_t hash = h;
    typedef Value type;
    typedef Tail tail;
};

template<hash_t hash, typename Head>
struct ListFind
{
    typedef typename std::conditional<hash == Head::hash, typename Head::type,
               typename ListFind<hash, typename Head::tail>::type>::type type;
};

template<hash_t hash>
struct ListFind<hash, EmptyNode>
{
    typedef void type;
};

/* end of list.h */

/*
 * let.h
 */

template<hash_t var, typename Value, typename Exp>
struct Let
{
    static_assert(var != 0);

    template<typename ValueType, typename List>
    struct Expr
    {
        typedef typename Value::template Expr<ValueType, List>::TrueType InVar;
        typedef typename Exp::template
                Expr<ValueType, Node<var, InVar, List> >::TrueType TrueType;
    };
};

/* end of let.h */

/*
 * ref.h
 */

template<hash_t var>
struct Ref
{
    static_assert(var != 0);

    template<typename ValueType, typename List>
    struct Expr
    {
        typedef typename ListFind<var, List>::type TrueType;
    };
};

/* end of ref.h */

/*
 * lambda.h
 */

template<hash_t var, typename Body>
struct Lambda
{
    template<typename ValueType, typename List>
    struct Expr : Function
    {
        typedef Expr<ValueType, List> TrueType;

        template<typename Param>
        using Fun = typename Body::template Expr<ValueType,
                    Node<var, typename Param::template Expr<ValueType, List>::TrueType, List>>::TrueType;
    };
};

/* end of lambda.h */

/*
 * invoke.h
 */

template<typename Fun, typename Param>
struct Invoke
{
    template<typename ValueType, typename List>
    struct Expr
    {
        static_assert(std::is_base_of<Function,
                      typename Fun::template Expr<ValueType, List>::TrueType>::value);

        typedef typename Fun::template Expr<ValueType, List>::TrueType::template Fun<Param> TrueType;
    };
};

/* end of invoke.h */

template<typename ValueType, bool b>
struct DoFibin
{
};

template<typename ValueType>
struct DoFibin<ValueType, true>
{
    template<typename Exp>
    static constexpr ValueType eval()
    {
        typedef typename Exp::template Expr<ValueType, EmptyNode>::TrueType E;

        static_assert(std::is_base_of<Expression<ValueType>, E>::value);

        return E::value();
    }
};

template<typename ValueType>
struct DoFibin<ValueType, false>
{
    template<typename Exp>
    static constexpr void eval()
    {
        std::cout << "Fibin doesn't support: "
                  << typeid(ValueType).name() << std::endl;
    }
};

template<bool b, typename T = void>
struct retType
{
    typedef void type;
};

template<typename ValueType>
struct retType<true, ValueType>
{
    typedef ValueType type;
};

template<typename ValueType>
struct Fibin
{
    template<typename Exp>
    static constexpr typename retType<std::is_integral<ValueType>::value, ValueType>::type eval()
    {
        return DoFibin<ValueType, std::is_integral<ValueType>::value>::template eval<Exp>();
    }
};

#endif /* FIBIN_H_ */
