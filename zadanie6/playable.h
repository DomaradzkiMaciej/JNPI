#ifndef PLAYABLE_H
#define PLAYABLE_H

#include <memory>

#include "file.h"

class Playable {
    friend class Playlist;

    [[nodiscard]] virtual bool contains(Playable *playable) const = 0;

public:
    virtual void play() = 0;
};

class Track : public Playable {
    [[nodiscard]] bool contains(Playable *playable) const override;
};

class Song : public Track {
public:
    explicit Song(const File &f);

    void play() override;

private:
    std::string artist;
    std::string title;
    std::string content;
};

class Movie : public Track {
public:
    explicit Movie(const File &f);

    void play() override;

private:
    std::string title;
    std::string year;
    std::string content;

    bool isYearCorrect();
};

#endif // PLAYABLE_H
