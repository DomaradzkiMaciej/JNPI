#include <algorithm>
#include <random>

#include "playbackMode.h"
#include "exceptions.h"

ShuffleMode::ShuffleMode(unsigned seed) : seed(seed) {}

std::vector<Playable*> Mode::createOrder(const order_t& playlist __attribute__((unused))) {
    throw UnsupportedPlaybackModeException {};
}

std::vector<Playable*> SequenceMode::createOrder(const order_t& playlist) {
    std::vector<Playable*> ret(playlist.size());
    std::transform(playlist.begin(), playlist.end(), ret.begin(),
        [](auto x) -> Playable* { return x.get(); });

    return ret;
}

std::vector<Playable*> OddEvenMode::createOrder(const order_t& playlist) {
    std::vector<Playable*> ret;
    ret.reserve(playlist.size());

    for (size_t i = 1; i < playlist.size(); i += 2) ret.push_back(playlist[i].get());
    for (size_t i = 0; i < playlist.size(); i += 2) ret.push_back(playlist[i].get());

    return ret;
}

std::vector<Playable*> ShuffleMode::createOrder(const order_t& playlist) {
    std::vector<Playable*> ret(playlist.size());
    std::transform(playlist.begin(), playlist.end(), ret.begin(),
        [](auto x) -> Playable* { return x.get(); });

    std::shuffle(ret.begin(), ret.end(), std::default_random_engine(seed));

    return ret;
}

std::unique_ptr<SequenceMode> createSequenceMode() {
    return std::make_unique<SequenceMode>();
}

std::unique_ptr<ShuffleMode> createShuffleMode(unsigned seed) {
    return std::make_unique<ShuffleMode>(seed);
}

std::unique_ptr<OddEvenMode> createOddEvenMode() {
    return std::make_unique<OddEvenMode>();
}
