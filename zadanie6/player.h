#ifndef PLAYER_H
#define PLAYER_H

#include "playlist.h"

class Player {
public:
    std::shared_ptr<Playable> openFile(const File& f);

    std::shared_ptr<Playlist> createPlaylist(const char *s);
};

#endif // PLAYER_H
