#ifndef FILE_H
#define FILE_H

#include <string>
#include <unordered_map>

class File {
public:
    File(const char *s);

    const std::string& getType() const noexcept;

    const std::string& getAttribute(const std::string& attrName) const;

    const std::string& getContent() const noexcept;

private:
    std::string type;
    std::unordered_map<std::string, std::string> attributes;
    std::string content;
};

#endif // FILE_H
