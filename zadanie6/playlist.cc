#include <algorithm>
#include <random>
#include <iostream>

#include "playlist.h"
#include "exceptions.h"

Playlist::Playlist(const char *s) : name(s), playbackMode(createSequenceMode()) {}

order_t::iterator Playlist::position_iter(size_t position) {
    if (position > elements.size()) {
        throw OutOfBoundsException{};
    }
    return elements.begin() + position;
}

void Playlist::add(const std::shared_ptr<Playable> &element) {
    add(element, elements.size());
}

void Playlist::add(const std::shared_ptr<Playable> &element, size_t position) {
    auto it = position_iter(position);
    if (element->contains(this)) throw PlaylistCycleException{};
    elements.insert(it, element);
}

void Playlist::remove() {
    if (elements.empty()) throw NoSuchElementException{};
    elements.pop_back();
}

void Playlist::remove(int position) {
    if (elements.empty()) throw NoSuchElementException{};
    auto it = position_iter(position);
    elements.erase(it);
}

bool Playlist::contains(Playable *playable) const {
    bool reachable = false;
    for (auto it = elements.begin(); it != elements.end() && !reachable; ++it) {
        reachable = (playable == it->get());
    }
    for (auto it = elements.cbegin(); it != elements.cend() && !reachable; ++it) {
        reachable = it->get()->contains(playable);
    }
    return reachable;
}

void Playlist::setMode(std::unique_ptr<Mode> mode) {
    playbackMode = std::move(mode);
}

void Playlist::play() {
    std::cout << "Playlist [" << name << "]" << std::endl;

    auto playlist = playbackMode->createOrder(elements);
    for (auto &element: playlist) {
        element->play();
    }
}
