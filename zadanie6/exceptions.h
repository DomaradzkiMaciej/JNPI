#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>

class PlayerException : public std::exception {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "player exception";
    }
};

class CorruptFileException : public PlayerException {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "corrupt file";
    }
};

class UnsupportedFileTypeException : public PlayerException {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "unsupported type";
    }
};

class NoSuchAttributeException : public PlayerException {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "required attribute not found";
    }
};

class CorruptContentException : public PlayerException {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "corrupt content";
    }
};

class NoSuchElementException : public PlayerException {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "no such element";
    }
};

class OutOfBoundsException : public PlayerException {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "out of bounds";
    }
};

class PlaylistCycleException : public PlayerException {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "playlist cycle";
    }
};

class InvalidYearException : public PlayerException {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "invalid year";
    }
};

class UnsupportedPlaybackModeException : public PlayerException {
public:
    [[nodiscard]] const char* what() const noexcept override {
        return "unsupported playback mode";
    }
};

#endif // EXCEPTIONS_H
