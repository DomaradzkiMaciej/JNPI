#include "file.h"
#include "exceptions.h"

File::File(const char *s) {
    const char separator = '|';
    size_t pos = 0, cur = 0;
    bool first = true;
    while (true) {
        size_t sep = 0;
        while (s[cur] != '\0' && s[cur] != separator) {
            if (s[cur] == ':' && sep == 0) sep = cur;
            cur++;
        }

        if (first) {
            if (s[cur] == '\0') throw CorruptFileException{};
            first = false;
            type = std::string{s + pos, s + cur};
        } else {
            if (s[cur] == '\0') {
                content = std::string(s + pos, s + cur);
                break;
            } else {
                if (sep == 0) throw CorruptFileException{};
                attributes.insert({
                    std::string{s + pos, s + sep},
                    std::string{s + sep + 1, s + cur}
                });
            }
        }

        if (s[cur] == '\0') break;
        pos = ++cur;
    }
}

const std::string& File::getType() const noexcept {
    return type;
}

const std::string& File::getContent() const noexcept {
    return content;
}

const std::string& File::getAttribute(const std::string &attrName) const {
    try {
        return attributes.at(attrName);
    } catch (std::out_of_range &e) {
        throw NoSuchAttributeException{};
    }
}
