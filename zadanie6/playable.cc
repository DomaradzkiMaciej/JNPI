#include "playable.h"
#include "exceptions.h"

#include <iostream>

namespace {
    const std::string &specChars() {
        const static std::string ret = ",.!?':;-";
        return ret;
    }

    bool isCorrect(char c) {
        return isalnum(static_cast<unsigned char>(c))
            || isspace(static_cast<unsigned char>(c))
            || specChars().find(c) != std::string::npos;
    }

    char rot13(char c) {
        if (c - 'A' < 13 || (c >= 'a' && c - 'a' < 13)) return c + 13;
        return c - 13;
    }

}

Song::Song(const File &f) {
    artist = f.getAttribute("artist");
    title = f.getAttribute("title");
    for (char c: f.getContent()) {
        if (!isCorrect(c)) {
            throw CorruptContentException {};
        }
    }
    content = f.getContent();
}

void Song::play() {
    std::cout << "Song [" << artist << ", " << title << "]: " << content << std::endl;
}

bool Movie::isYearCorrect() {
    if (year.length() == 0) return false;
    if (year[0] == '-') {
        if (year.length() == 1) return false;
        if (year[1] == '0') return false;
    } else {
        if (year[0] == '0') return false;
        if (!isdigit(static_cast<unsigned char>(year[0]))) return false;
    }

    for (size_t i = 1; i < year.length(); ++i) {
        if (!isdigit(static_cast<unsigned char>(year[i]))) return false;
    }

    return true;
}

Movie::Movie(const File &f) {
    title = f.getAttribute("title");
    year = f.getAttribute("year");

    if (!isYearCorrect()) throw InvalidYearException {};

    content.reserve(f.getContent().length());
    for (char c: f.getContent()) {
        if (!isCorrect(c)) throw CorruptContentException {};

        if (isalpha(c)) content.append(1, rot13(c));
        else content.append(1, c);
    }
}

void Movie::play() {
    std::cout << "Movie [" << title << ", " << year << "]: " << content << std::endl;
}

bool Track::contains(Playable *playable __attribute__((unused))) const { return false; }
