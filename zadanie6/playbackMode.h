#ifndef PLAYBACKMODE_H
#define PLAYBACKMODE_H

#include <vector>

#include "playable.h"

typedef std::vector<std::shared_ptr<Playable>> order_t;

class Mode {
public:
    virtual std::vector<Playable*> createOrder(const order_t& playlist);
};

class SequenceMode : public Mode {
public:
    std::vector<Playable*> createOrder(const order_t& playlist) override;
};

class OddEvenMode : public Mode {
public:
    std::vector<Playable*> createOrder(const order_t& playlist) override;
};

class ShuffleMode : public Mode {
public:
    explicit ShuffleMode(unsigned seed);

    std::vector<Playable*> createOrder(const order_t& playlist) override;

private:
    unsigned seed;
};


std::unique_ptr<SequenceMode> createSequenceMode();

std::unique_ptr<ShuffleMode> createShuffleMode(unsigned seed);

std::unique_ptr<OddEvenMode> createOddEvenMode();


#endif // PLAYBACKMODE_H
