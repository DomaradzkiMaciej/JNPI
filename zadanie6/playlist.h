#ifndef PLAYLIST_H
#define PLAYLIST_H

#include "playbackMode.h"

class Playlist : public Playable {
    std::string name;
    std::vector<std::shared_ptr<Playable>> elements;
    std::unique_ptr<Mode> playbackMode;

    order_t::iterator position_iter(size_t position) noexcept(false);

    [[nodiscard]] bool contains(Playable *playable) const override;

public:
    explicit Playlist(const char *s);

    void add(const std::shared_ptr<Playable> &element) noexcept(false);

    void add(const std::shared_ptr<Playable> &element, size_t position) noexcept(false);

    void remove() noexcept(false);

    void remove(int position) noexcept(false);

    void setMode(std::unique_ptr<Mode> mode);

    void play() override;
};


#endif // PLAYLIST_H
