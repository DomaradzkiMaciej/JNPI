#include "player.h"
#include "exceptions.h"

std::shared_ptr<Playable> Player::openFile(const File &f) {
    if (f.getType() == "") throw CorruptFileException {};
    if (f.getType() == "audio") return std::make_shared<Song>(f);
    if (f.getType() == "video") return std::make_shared<Movie>(f);
    throw UnsupportedFileTypeException {};
}

std::shared_ptr<Playlist> Player::createPlaylist(const char *s) {
    return std::make_shared<Playlist>(s);
}