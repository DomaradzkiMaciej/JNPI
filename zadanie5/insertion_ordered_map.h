#ifndef INSERTION_ORDERED_MAP_INSERTION_ORDERED_MAP_H
#define INSERTION_ORDERED_MAP_INSERTION_ORDERED_MAP_H

#include <list>
#include <unordered_map>
#include <algorithm>
#include <memory>

class lookup_error : public std::exception {
    const char *what() const noexcept override {
        return "lookup_error";
    }
};

template<class K, class V, class Hash = std::hash<K>>
class insertion_ordered_map {
    using my_list = std::list<std::pair<K, V>>;
    using my_map = std::unordered_map<K,
            typename std::list<std::pair<K, V>>::iterator, Hash>;

private:
    std::shared_ptr<my_list> list;

    std::shared_ptr<my_map> map;

    void copy_needed();

public:

    insertion_ordered_map();

    insertion_ordered_map(insertion_ordered_map const &other) noexcept;

    insertion_ordered_map(insertion_ordered_map &&other) noexcept;


    auto &operator=(insertion_ordered_map other) noexcept;

    bool empty() const noexcept;

    size_t size() const noexcept;

    void clear();

    bool contains(K const &k) const;

    bool insert(K const &k, V const &v);

    void erase(K const &k);

    void merge(insertion_ordered_map const &other);

    V &at(K const &k);

    V const &at(K const &k) const;

    V &operator[](K const &k);

    // klasa iteratora
    class iterator {
    public:
        typename std::list<std::pair<K, V>>::const_iterator itr;

        // konstruktor bezparametrowy
        iterator() {}

        // konstruktor kopiujący
        iterator(insertion_ordered_map::iterator const& other) {
            this->itr = other.itr;
        }

        std::pair<K, V> const& operator* () {
            return *itr;
        }

        iterator& operator++ () {
            ++itr;
            return *this;
        }

        bool operator== (const iterator& other) {
            if (itr == other.itr)
                return true;
            else
                return false;
        }

        bool operator!= (const iterator& other) {
            if (itr != other.itr)
                return true;
            else
                return false;
        }

        std::pair<K, V> const *operator->() {
            return &(*itr);
        }

    };

    // Metody begin i end pozwlające na przeglądanie zbioru kluczy.
    iterator begin() const {
        iterator cover;
        cover.itr = this->list->cbegin();
        return cover;
    }

    iterator end() const {
        iterator cover;
        cover.itr = this->list->cend();
        return cover;
    }

};
// Ta funkcja sprawdza czy istnieje potrzeba utworzenia kopi mapy
// (kiedy istnieją inne mapy z wskaźnikiem na te same obiekty)
// i jeśli taka potrzeba zachodzi tworzy kopię.
template<class K, class V, class Hash>
void insertion_ordered_map<K, V, Hash>::copy_needed() {
    auto map_backup = map;
    auto list_backup = list;
    try {
        if (map.use_count() > 1) {
            list = std::make_shared<my_list>(*list);
            map = std::make_shared<my_map>();
            auto it = list->begin();
            while (it != list->end()) {
                map->emplace(it->first, it);
                it++;
            }
        }
    } catch (std::exception &e) {
        list = list_backup;
        map = map_backup;

        throw e;
    }
}

// konstruktor bezparametrowy
template<class K, class V, class Hash>
insertion_ordered_map<K, V, Hash>::insertion_ordered_map():
        list(std::make_shared<my_list>()), map(std::make_shared<my_map>()) {}

// konstruktor kopiujący
template<class K, class V, class Hash>
insertion_ordered_map<K, V, Hash>::insertion_ordered_map(
    insertion_ordered_map const& other) noexcept:
        list(std::make_shared<my_list>(* other.list.get())),
            map(std::make_shared<my_map>(* other.map.get())) {}

// konstruktor przenoszący
template<class K, class V, class Hash>
insertion_ordered_map<K, V, Hash>::insertion_ordered_map(
        insertion_ordered_map &&other) noexcept :list(
        std::move(other.list)), map(std::move(other.map)) {}

// operator przypisania
template<class K, class V, class Hash>
auto &
insertion_ordered_map<K, V, Hash>::operator=(insertion_ordered_map other)
noexcept {
    if (this != &other) {
        list = other.list;
        map = other.map;
    }

    return *this;
}

// sprawdzenie niepustości słownika
template<class K, class V, class Hash>
bool insertion_ordered_map<K, V, Hash>::empty() const noexcept {
    return list->empty();
}

// rozmiar słownika
template<class K, class V, class Hash>
size_t insertion_ordered_map<K, V, Hash>::size() const noexcept {
    return list->size();
}

// czyszczenie słownika
template<class K, class V, class Hash>
void insertion_ordered_map<K, V, Hash>::clear() {
    auto list_before = list;
    auto map_before = map;

    try {
        if (map.use_count() == 1) {
            list->clear();
            map->clear();
        } else {
            list = std::make_shared<my_list>();
            map = std::make_shared<my_map>();
        }
    } catch (std::exception &e) {
        list = list_before;
        map = map_before;

        throw e;
    }
}

// sprawdzenie klucza
template<class K, class V, class Hash>
bool insertion_ordered_map<K, V, Hash>::contains(const K &k) const {
    return map->find(k) != map->end();
}

// wstawianie elementów
template<class K, class V, class Hash>
bool insertion_ordered_map<K, V, Hash>::insert(K const &k, V const &v) {
    copy_needed();  //kopiowanie w razie potrzeby
    auto check = map->find(k);
    if (check != map->end()) {
        list->erase(check->second);
        list->push_back(std::make_pair(k, v));
        return false;
    } else {
        list->push_back(std::make_pair(k, v));
        auto iter = list->end();
        iter--;
        map->emplace(k, iter);
        return true;
    }
}

// usuwanie elementów
template<class K, class V, class Hash>
void insertion_ordered_map<K, V, Hash>::erase(K const &k) {
    copy_needed();  //kopiowanie w razie potrzeby
    auto check = map->find(k);
    if (check != map->end()) {
        list->erase(check->second);
        map->erase(check);
    } else {
        throw lookup_error();
    }
}

// scalanie map
template<class K, class V, class Hash>
void
insertion_ordered_map<K, V, Hash>::merge(insertion_ordered_map const &other) {
    copy_needed();  //zakładam że kopiowanie jest tu potrzebne
    auto it = other.list->begin();
    while (it != other.list->end()) {
        auto check = this->map->find(it->first);
        if (check == map->end()) {
            this->list->push_back(std::make_pair(it->first, it->second));
            auto iter = this->list->end();
            iter--;
            this->map->emplace(it->first, iter);
        }
        it++;
    }
}

// referencja wartości
template<class K, class V, class Hash>
V& insertion_ordered_map<K, V, Hash>::at(K const &k) {
    copy_needed();  //kopiowanie w razie potrzeby
    auto check = map->find(k);
    if (check != map->end()) {
        return map->at(k)->second;
    } else {
        throw lookup_error();
    }
}

// referencja wartości w wersji const
template<class K, class V, class Hash>
const V& insertion_ordered_map<K, V, Hash>::at(K const &k) const {
    auto check = map->find(k);
    if (check != map->end()) {
        return map->at(k)->second;
    } else {
        throw lookup_error();
    }
}

// operator indeksowania
template<class K, class V, class Hash>
V& insertion_ordered_map<K, V, Hash>::operator[](K const &k) {
    copy_needed();  //kopiowanie w razie potrzeby
    auto check = map->find(k);
    if (check != map->end()) {
        return map->at(k)->second;
    } else {
        list->push_back(std::make_pair(k, V()));
        auto iter = list->end();
        iter--;
        map->emplace(k, iter);
        return iter->second;
    }
}

#endif //INSERTION_ORDERED_MAP_INSERTION_ORDERED_MAP_H
