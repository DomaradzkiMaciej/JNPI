#include "poset.h"
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <string>
#include <iostream>

#ifndef NDEBUG
const bool debug = true;
#else
const bool debug = false;
#endif

/** Zbiór elementów, reprezentowanych przez ich skopiowane nazwy. */
using Set = std::unordered_set<std::string>;
/** Zbiór relacji. Każda relacja jest reprezentowana przez parę wskaźników na
  * skopiowane nazwy tych elementów. */
using Relations = std::set<std::pair<const std::string *, const std::string *>>;
/** Zbiór częściowo uporządkowany to para
  * (zbiór elementów, zbiór relacji między nimi). */
using Poset = std::pair<Set, Relations>;
/** Każdy zbiór częściowo uporządkowany jest skojarzony ze swoim numerem za
  * pomocą mapy. */
using PosetSet = std::unordered_map<unsigned long, Poset>;

namespace {
    PosetSet &posets() {
        static PosetSet ans;

        return ans;
    }
}

unsigned long jnp1::poset_new() {
    if (debug)
        std::cerr << "poset_new()\n";

    static unsigned long last_id = 0;

    posets().emplace(last_id, Poset());

    if (debug)
        std::cerr << "poset_new: poset " << last_id << " created\n";

    return last_id++;
}

void jnp1::poset_delete(unsigned long id) {
    if (debug)
        std::cerr << "poset_delete(" << id << ")\n";

    auto poset_iter = posets().find(id);

    if (poset_iter == posets().end()) {
        if (debug)
            std::cerr << "poset_delete: poset " << id << " does not exist\n";

        return;
    }

    posets().erase(poset_iter);

    if (debug)
        std::cerr << "poset_delete: poset " << id << " deleted\n";
}

size_t jnp1::poset_size(unsigned long id) {
    if (debug)
        std::cerr << "poset_size(" << id << ")\n";

    size_t size;

    try {
        size = posets().at(id).first.size();

        if (debug)
            std::cerr << "poset_size: poset " << id << " contains " << size
                      << " element(s)\n";

        return size;
    }
    catch (const std::out_of_range &) {
        if (debug)
            std::cerr << "poset_size: poset " << id << " does not exist\n";

        return 0;
    }
}

bool jnp1::poset_insert(unsigned long id, char const *value) {
    if (debug) {
        if (value == nullptr)
            std::cerr << "poset_insert(" << id << R"(, "NULL"))" << "\n";
        else
            std::cerr << "poset_insert(" << id << ", \"" << value << "\")\n";
    }

    auto poset_iter = posets().find(id);

    if (debug) {
        if (value == nullptr)
            std::cerr << "poset_insert: invalid value (NULL)\n";
        if (poset_iter == posets().end())
            std::cerr << "poset_insert: poset " << id << " does not exist\n";
    }

    if (poset_iter == posets().end() || value == nullptr)
        return false;

    /* Ciąg znaków value jest kopiowany do kontenera string i zapysiwany
     * w odpowiednim zbiorze elementów. */
    auto result = poset_iter->second.first.insert(std::string(value));

    if (result.second) {
        // Pobieranie wskaźnika na zapisany w zbiorze nowy element.
        const std::string *reference = &*result.first;

        // Umieszczenie relacji zwrotnej w zbiorze relacji.
        poset_iter->second.second.emplace(reference, reference);

        if (debug)
            std::cerr << "poset_insert: poset " << id << ", element \""
                      << value << "\" inserted\n";

        return true;
    }

    if (debug)
        std::cerr << "poset_insert: poset " << id << ", element \"" << value
                  << "\" already exists\n";

    return false;
}

bool jnp1::poset_remove(unsigned long id, char const *value) {
    if (debug) {
        if (value == nullptr)
            std::cerr << "poset_remove(" << id << R"(, "NULL"))" << "\n";
        else
            std::cerr << "poset_remove(" << id << ", \"" << value << "\")\n";
    }

    auto poset_iter = posets().find(id);

    if (debug) {
        if (value == nullptr)
            std::cerr << "poset_remove: invalid value (NULL)\n";
        if (poset_iter == posets().end())
            std::cerr << "poset_remove: poset " << id << " does not exist\n";
    }

    if (poset_iter == posets().end() || value == nullptr)
        return false;

    // Znalezenie iteratora w zbiorze elementów, wskazującego na nazwę value.
    auto value_iter = poset_iter->second.first.find(std::string(value));

    if (value_iter == poset_iter->second.first.end()) {
        if (debug)
            std::cerr << "poset_remove: poset " << id << ", element \""
                      << value << "\" does not exist\n";

        return false;
    }

    /* Usuwanie wszystkich relacji danego elementu ze zbioru relacji.
     * Każda para typu (dany element, element ze bioru elementów) lub
     * (element ze zbioru elementów, dany element) jest brana pod uwagę.
     */
    for (auto const &other : poset_iter->second.first) {
        poset_iter->second.second.erase(make_pair(&*value_iter, &other));
        poset_iter->second.second.erase(make_pair(&other, &*value_iter));
    }

    poset_iter->second.first.erase(value_iter);

    if (debug)
        std::cerr << "poset_remove: poset " << id << ", element \"" << value
                  << "\" removed\n";

    return true;
}

bool jnp1::poset_add(unsigned long id, char const *value1, char const *value2) {
    if (debug) {
        std::cerr << "poset_add(" << id;
        if (value1 == nullptr && value2 == nullptr)
            std::cerr << R"(, "NULL", "NULL"))" << "\n";
        else if (value1 == nullptr)
            std::cerr << R"(, "NULL", ")" << value2 << "\")\n";
        else if (value2 == nullptr)
            std::cerr << ", \"" << value1 << R"(", "NULL"))" << "\n";
        else
            std::cerr << ", \"" << value1 << "\", \"" << value2 << "\")\n";
    }

    auto poset_iter = posets().find(id);

    if (debug) {
        if (value1 == nullptr)
            std::cerr << "poset_add: invalid value1 (NULL)\n";

        if (value2 == nullptr)
            std::cerr << "poset_add: invalid value2 (NULL)\n";

        if (poset_iter == posets().end())
            std::cerr << "poset_add: poset " << id << " does not exist\n";
    }

    if (poset_iter == posets().end() || value1 == nullptr || value2 == nullptr)
        return false;

    // Znalezienie iteratora w zbiorze elementów, wskazującego na nazwę value1.
    auto value1_iter = poset_iter->second.first.find(std::string(value1));

    // Sprawdzenie, czy nazwa value1 jest w zbiorze.
    if (value1_iter == poset_iter->second.first.end()) {
        if (debug)
            std::cerr << "poset_add: poset " << id << ", element \"" << value1
                      << "\" or \"" << value2 << "\" does not exist\n";

        return false;
    }

    // Znalezienie iteratora w zbiorze elementów, wskazującego na nazwę value2.
    auto value2_iter = poset_iter->second.first.find(std::string(value2));

    // Sprawdzenie, czy nazwa value2 jest w zbiorze.
    if (value2_iter == poset_iter->second.first.end()) {
        if (debug)
            std::cerr << "poset_add: poset " << id << ", element \"" << value1
                      << "\" or \"" << value2 << "\" does not exist\n";

        return false;
    }

    // Sprawdzenie, czy relacja (value1, value2) jest w zbiorze relacji.
    if (poset_iter->second.second.count(
            make_pair(&*value1_iter, &*value2_iter))) {
        if (debug)
            std::cerr << "poset_add: poset " << id << ", element \"" << value1
                      << "\" or \"" << value2 << "\" cannot be added\n";

        return false;
    }

    // Sprawdzenie, czy relacja (value2, value1) jest w zbiorze relacji.
    if (poset_iter->second.second.count(
            make_pair(&*value2_iter, &*value1_iter))) {
        if (debug)
            std::cerr << "poset_add: poset " << id << ", element \"" << value1
                      << "\" or \"" << value2 << "\" cannot be added\n";

        return false;
    }

    /* Domykanie relacji przechodniej.
     * Każda para (minor, major) elementów ze zbioru potencjalnie może być
     * w zbiorze relacji po domknięciu relacji przechodniu. Dlatego sprawdzana
     * jest każda, a relacja jest dodawany wtedy i tylko wtedy, gdy zachodzi
     * minor <= value1 <= value2 <= major. */
    for (auto const &minor : poset_iter->second.first) {
        for (auto const &major : poset_iter->second.first) {
            if (poset_iter->second.second.count(
                    make_pair(&minor, &*value1_iter)) &&
                poset_iter->second.second.count(
                        make_pair(&*value2_iter, &major))) {
                poset_iter->second.second.insert(make_pair(&minor, &major));
            }
        }
    }

    if (debug)
        std::cerr << "poset_add: poset " << id << ", relation (\"" << value1
                  << "\", \"" << value2 << "\") added\n";

    return true;
}

bool jnp1::poset_del(unsigned long id, char const *value1, char const *value2) {
    if (debug) {
        if (value1 == nullptr && value2 == nullptr)
            std::cerr << "poset_del(" << id << R"(, "NULL", "NULL"))" << "\n";
        else if (value1 == nullptr)
            std::cerr << "poset_del(" << id << R"(, "NULL", ")" << value2
                      << "\")\n";
        else if (value2 == nullptr)
            std::cerr << "poset_del(" << id << ", \"" << value1
                      << R"(", "NULL"))" << "\n";
        else
            std::cerr << "poset_del(" << id << ", \"" << value1 << "\", \""
                      << value2 << "\")\n";
    }

    auto poset_iter = posets().find(id);

    if (debug) {
        if (value1 == nullptr)
            std::cerr << "poset_del: invalid value1 (NULL)\n";

        if (value2 == nullptr)
            std::cerr << "poset_del: invalid value2 (NULL)\n";

        if (poset_iter == posets().end())
            std::cerr << "poset_del: poset " << id << " does not exist\n";
    }

    if (poset_iter == posets().end() || value1 == nullptr || value2 == nullptr)
        return false;

    // Znalezienie iteratora w zbiorze elementów, wskazującego na nazwę value1.
    auto value1_iter = poset_iter->second.first.find(std::string(value1));

    if (value1_iter == poset_iter->second.first.end()) {
        if (debug)
            std::cerr << "poset_del: poset 0, element \"" << value1
                      << "\" or \"" << value2 << "\" does not exist\n";

        return false;
    }

    // Znalezienie iteratora w zbiorze elementów, wskazującego na nazwę value2.
    auto value2_iter = poset_iter->second.first.find(std::string(value2));

    if (value2_iter == poset_iter->second.first.end()) {
        if (debug)
            std::cerr << "poset_del: poset 0, element \"" << value1
                      << "\" or \"" << value2 << "\" does not exist\n";

        return false;
    }

    // Sprawdzanie, czy para (value1, value2) nie należy do zbioru relacji.
    if (!poset_iter->second.second.count(
            make_pair(&*value1_iter, &*value2_iter))) {
        if (debug)
            std::cerr << "poset_del: poset " << id << ", relation (" << value1
                      << ", " << value2 << ") does not exist\n";

        return false;
    }

    if (value1_iter == value2_iter)
        return false;

    /* Sprawdzanie, czy usunięcie relacji między elementami nie spowoduje, że
     * relacja przestanie być przechodnia, tzn. czy istnieje element ze zbioru
     * elementów różny od value1 i value2, że value1 <= ten element <= value2. */
    for (auto const &other : poset_iter->second.first) {
        if (other == *value1_iter || other == *value2_iter)
            continue;

        if (poset_iter->second.second.count(make_pair(&*value1_iter, &other)) &&
            poset_iter->second.second.count(make_pair(&other, &*value2_iter))) {
            if (debug)
                std::cerr << "poset_del: poset " << id << ", relation ("
                          << value1 << ", " << value2 << ") cannot be deleted\n";

            return false;
        }
    }

    poset_iter->second.second.erase(make_pair(&*value1_iter, &*value2_iter));

    if (debug)
        std::cerr << "poset_del: poset " << id << ", relation (" << value1
                  << ", " << value2 << ") added\n";

    return true;
}

bool
jnp1::poset_test(unsigned long id, char const *value1, char const *value2) {
    if (debug) {
        if (value1 == nullptr && value2 == nullptr)
            std::cerr << "poset_test(" << id << R"(, "NULL", "NULL"))" << "\n";
        else if (value1 == nullptr)
            std::cerr << "poset_test(" << id << R"(, "NULL", ")" << value2
                      << "\")\n";
        else if (value2 == nullptr)
            std::cerr << "poset_test(" << id << ", \"" << value1
                      << R"(", "NULL"))" << "\n";
        else
            std::cerr << "poset_test(" << id << ", \"" << value1 << "\", \""
                      << value2 << "\")\n";
    }

    auto poset_iter = posets().find(id);

    if (debug) {
        if (value1 == nullptr)
            std::cerr << "poset_test: invalid value1 (NULL)\n";

        if (value2 == nullptr)
            std::cerr << "poset_test: invalid value2 (NULL)\n";

        if (poset_iter == posets().end())
            std::cerr << "poset_test: poset " << id << " does not exist\n";
    }

    if (poset_iter == posets().end() || value1 == nullptr || value2 == nullptr)
        return false;

    // Znalezienie iteratora w zbiorze elementów, wskazującego na nazwę value1.
    auto value1_iter = poset_iter->second.first.find(std::string(value1));

    if (value1_iter == poset_iter->second.first.end()) {
        if (debug)
            std::cerr << "poset_test: poset 0, element \"" << value1
                      << "\" or \"" << value2 << "\" does not exist\n";

        return false;
    }

    // Znalezienie iteratora w zbiorze elementów, wskazującego na nazwę value2.
    auto value2_iter = poset_iter->second.first.find(std::string(value2));

    if (value2_iter == poset_iter->second.first.end()) {
        if (debug)
            std::cerr << "poset_test: poset 0, element \"" << value1
                      << "\" or \"" << value2 << "\" does not exist\n";

        return false;
    }

    // Sprawdzanie, czy relacja (value1, value2) jest w zbiorze relacji.
    bool do_exist = poset_iter->second.second.count(
            make_pair(&*value1_iter, &*value2_iter));

    if (debug) {
        if (do_exist)
            std::cerr << "poset_test: poset " << id << ", relation (\""
                      << value1 << "\", \"" << value2 << "\") exists\n";
        else
            std::cerr << "poset_test: poset " << id << ", relation (\""
                      << value1 << "\", \"" << value2 << "\") does not exists\n";
    }

    return do_exist;
}

void jnp1::poset_clear(unsigned long id) {
    if (debug)
        std::cerr << "poset_clear(" << id << ")\n";

    auto poset_iter = posets().find(id);

    if (poset_iter == posets().end()) {
        if (debug)
            std::cerr << "poset_clear: poset " << id << " does not exist\n";

        return;
    }

    poset_iter->second.second.clear();
    poset_iter->second.first.clear();

    if (debug)
        std::cerr << "poset_clear: poset " << id << " cleared\n";
}
